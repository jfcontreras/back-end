package com.colfondos.example.controller;

import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.colfondos.example.exception.ResourceNotFoundException;
import com.colfondos.example.model.Customer;
import com.colfondos.example.model.Document;
import com.colfondos.example.repository.CustomerRepository;
import com.colfondos.example.repository.DocumentRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/v1")
public class CustomerController {
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private DocumentRepository documentRepository;

	@GetMapping("/customer")
	public Page<Customer> getAllcustomer(Pageable pageable) {
		return customerRepository.findAll(pageable);
	}

	@GetMapping("/customer/{id}")
	public ResponseEntity<Customer> getCustomerById(@PathVariable(value = "id") Long customerId)
			throws ResourceNotFoundException {
		Customer Customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new ResourceNotFoundException("Customer not found for this id :: " + customerId));
		return ResponseEntity.ok().body(Customer);
	}

	@PostMapping("/customer")
	public Customer createCustomer(@Valid @RequestBody Customer Customer) {
		Optional<Document> d = documentRepository.findById(Customer.getDocument().getId());
		Customer.setDocument(d.get());
		return customerRepository.save(Customer);
	}

	@PutMapping("/customer/{id}")
	public ResponseEntity<Customer> updateCustomer(@PathVariable(value = "id") Long customerId,
			@Valid @RequestBody Customer CustomerDetails) throws ResourceNotFoundException {
		Customer Customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new ResourceNotFoundException("Customer not found for this id :: " + customerId));

		Customer.setEmailId(CustomerDetails.getEmailId());
		Customer.setLastName(CustomerDetails.getLastName());
		Customer.setFirstName(CustomerDetails.getFirstName());
		final Customer updatedCustomer = customerRepository.save(Customer);
		return ResponseEntity.ok(updatedCustomer);
	}

	@DeleteMapping("/customer/{id}")
	public Map<String, Boolean> deleteCustomer(@PathVariable(value = "id") Long customerId)
			throws ResourceNotFoundException {
		Customer Customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new ResourceNotFoundException("Customer not found for this id :: " + customerId));

		customerRepository.delete(Customer);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
