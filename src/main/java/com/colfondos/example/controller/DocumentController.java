package com.colfondos.example.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.colfondos.example.exception.ResourceNotFoundException;
import com.colfondos.example.model.Customer;
import com.colfondos.example.model.Document;
import com.colfondos.example.repository.DocumentRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/v1")
public class DocumentController {
	@Autowired
	private DocumentRepository documentRepository;

	@GetMapping("/document")
	public Page<Document> getAllDocument(Pageable pageable) {
		return documentRepository.findAll(pageable);
	}
	
	
	@PostMapping("/document")
	public Document createDocumento(@Valid @RequestBody Document document) {
		return documentRepository.save(document);
	}
	
	@PutMapping("/document/{id}")
	public Document updateDocumento(@Valid @RequestBody Document document) {
		return documentRepository.save(document);
	}
	
	@DeleteMapping("/document/{id}")
	public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long customerId)
			throws ResourceNotFoundException {
		Document employee = documentRepository.findById(customerId)
				.orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + customerId));

		documentRepository.delete(employee);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}


}
