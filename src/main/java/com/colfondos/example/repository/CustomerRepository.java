package com.colfondos.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.colfondos.example.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{

}
